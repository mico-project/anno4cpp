package eu.mico.platform.jni;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import com.beust.jcommander.JCommander;
import com.github.anno4j.model.impl.selector.FragmentSelector;
import eu.mico.platform.anno4j.model.impl.bodymmm.TVSShotBodyMMM;
import eu.mico.platform.anno4j.model.impl.bodymmm.TVSShotBoundaryFrameBodyMMM;
import eu.mico.platform.persistence.impl.IDGeneratorAnno4j;

import org.openrdf.repository.RepositoryException;

import org.openrdf.model.impl.URIImpl;

import com.github.anno4j.Anno4j;

import eu.mico.platform.anno4j.model.AssetMMM;
import eu.mico.platform.anno4j.model.ItemMMM;
import eu.mico.platform.anno4j.model.PartMMM;

import eu.mico.platform.anno4j.model.impl.bodymmm.AudioDemuxBodyMMM;
import eu.mico.platform.anno4j.model.impl.targetmmm.SpecificResourceMMM;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.sparql.SPARQLRepository;

public class Anno4JDependencies {

    /**
     * @param args
     * @throws URISyntaxException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws RepositoryException
     */
    public static void main(String[] args) throws IOException, URISyntaxException, RepositoryException, IllegalAccessException, InstantiationException {

        Anno4JDependenciesCLI cli = null;

        try {
            cli = new Anno4JDependenciesCLI();
            JCommander cmd = new JCommander(cli, args);

            if (cli.isA4jversion() && cli.isVersion()) {
                cmd.usage();
                System.exit(-1);
            }

            if (cli.isVersion() || cli.isA4jversion()) {
                String versionToPrint = "";
                Properties props = new Properties();
                try
                {
                    props.load(Anno4JDependencies.class.getResourceAsStream("/.properties"));
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                if (cli.isVersion())
                    versionToPrint = props.getProperty("anno4jdependencies.version");

                if (cli.isA4jversion())
                    versionToPrint = props.getProperty("anno4jdependencies.anno4jversion");

                System.out.println(versionToPrint);
                System.exit(0);
            }

        } catch (com.beust.jcommander.ParameterException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        URI marmottaBaseURI = new URI("http://localhost:8080/marmotta");

        IDGeneratorAnno4j idGenerator = new IDGeneratorAnno4j(marmottaBaseURI.toString());
        SPARQLRepository sparqlRepository =
                new SPARQLRepository(marmottaBaseURI.toString() + "/sparql/select",
                        marmottaBaseURI.toString() + "/sparql/update");

        Anno4j anno4j = null;
        try {
            anno4j = new Anno4j(sparqlRepository, idGenerator);
        } catch (RepositoryConfigException e) {
            throw new IllegalStateException("Couldn\'t instantiate Anno4j: " + e.toString());
        }

        //---------------------------------------------
        //      input created by the inject tool:
        //---------------------------------------------

        ItemMMM  itemMMM=anno4j.createObject(ItemMMM.class);

        //Every Item MUST have an annotated asset
        AssetMMM assetMMM=anno4j.createObject(AssetMMM.class);
        assetMMM.setFormat("video/mp4");
        assetMMM.setLocation("some/location");

        itemMMM.setAsset(assetMMM);

        // call persist to move item and all generated annotations to corresponding sub-graph
        URIImpl contextfromInject = new URIImpl(itemMMM.getResourceAsString());
        anno4j.persist(itemMMM, contextfromInject);

        //---------------------------------------------
        //      annotation created by the extractor:
        //---------------------------------------------

        AssetMMM wavAssetMMM=anno4j.createObject(AssetMMM.class);
        wavAssetMMM.setFormat("audio/wav");
        wavAssetMMM.setLocation("some/other/location");


        //every Item should have at least one Part
        PartMMM  partMMM=anno4j.createObject(PartMMM.class);
        partMMM.setAsset(wavAssetMMM);

        //every part MUST have a body
        AudioDemuxBodyMMM bodyMMM=anno4j.createObject(AudioDemuxBodyMMM.class);
        bodyMMM.setFrameRate("8000");

        //every part MUST (?) have a target with a source
        SpecificResourceMMM targetMMM=anno4j.createObject(SpecificResourceMMM.class);
        targetMMM.setSource(itemMMM);

        //add to the part the body, the input resource (in this case the item, but may be a part), and the target
        partMMM.setBody(bodyMMM);
        partMMM.addInput(itemMMM);
        partMMM.addTarget(targetMMM);

        //finally link everything to the input
        itemMMM.addPart(partMMM);

        //move the part(s) to the same context of the input
        URIImpl context = new URIImpl(itemMMM.getResourceAsString());
        anno4j.persist(itemMMM, context);

        //---------------------------------------------
        //      complex annotation created by the TVS
        //---------------------------------------------

        ItemMMM tvsItemMMM=anno4j.createObject(ItemMMM.class);
        URIImpl tvsContext = new URIImpl(tvsItemMMM.getResourceAsString());

        AssetMMM videoAssetMMM=anno4j.createObject(AssetMMM.class);
        videoAssetMMM.setFormat("video/mp4");
        videoAssetMMM.setLocation("path/to/the/storage/video");

        tvsItemMMM.setAsset(videoAssetMMM);

        // lets say the video consists of three shots
        double shots[][] = {{0,1000}, {1000,7240}, {7240,9240}};

        for (int i=0; i<3; i++) {

            //+++++ create a part for the shot annotation (no media produced) +++++
            PartMMM             shotPartMMM  = anno4j.createObject(PartMMM.class,tvsContext);
            SpecificResourceMMM shotTarget   = anno4j.createObject(SpecificResourceMMM.class,tvsContext);
            FragmentSelector    shotSelector = anno4j.createObject(FragmentSelector.class,tvsContext);

            shotSelector.setTemporalFragment(shots[i][0],shots[i][1]);
            shotTarget.setSelector(shotSelector);

            //set body and target
            shotPartMMM.setBody(anno4j.createObject(TVSShotBodyMMM.class));
            shotPartMMM.addTarget(shotTarget);

            //provenance
            //this part used the video item as input
            shotPartMMM.addInput(tvsItemMMM);
            //the parts target had the video item as as source
            shotTarget.setSource(tvsItemMMM);

            //+++++ create a part for the shot boundary frame (image media produced) ++++
            TVSShotBoundaryFrameBodyMMM shotBoundaryFrameBodyMMM = anno4j.createObject(TVSShotBoundaryFrameBodyMMM.class,tvsContext);

            PartMMM             shotBoundaryPartMMM  = anno4j.createObject(PartMMM.class,tvsContext);
            SpecificResourceMMM shotBoundaryTarget   = anno4j.createObject(SpecificResourceMMM.class,tvsContext);
            FragmentSelector    shotBoundarySelector = anno4j.createObject(FragmentSelector.class,tvsContext);

            //the temporal fragment has the length of one frame (here @25fps) which starts at the beginning of the shot
            shotBoundarySelector.setTemporalFragment(shots[i][0],shots[i][0]+40);
            shotBoundaryTarget.setSelector(shotBoundarySelector);

            //add the Asset for the created image asset to the part
            AssetMMM shotBoundaryFrameAsset = anno4j.createObject(AssetMMM.class,tvsContext);
            shotBoundaryFrameAsset.setFormat("image/png");
            shotBoundaryFrameAsset.setLocation("path/to/the/storage/image");
            shotBoundaryPartMMM.setAsset(shotBoundaryFrameAsset);

            //set body and target
            shotBoundaryPartMMM.setBody(anno4j.createObject(TVSShotBoundaryFrameBodyMMM.class,tvsContext));
            shotBoundaryPartMMM.addTarget(shotBoundaryTarget);

            //provenance
            //this part used the video item as input
            shotBoundaryPartMMM.addInput(tvsItemMMM);
            //the parts target had the video item as as source
            shotBoundaryTarget.setSource(tvsItemMMM);


            //add the two annotation parts to the item
            tvsItemMMM.addPart(shotPartMMM);
            tvsItemMMM.addPart(shotBoundaryPartMMM);

        }

    }

}

