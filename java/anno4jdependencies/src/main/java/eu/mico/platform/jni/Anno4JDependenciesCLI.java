package eu.mico.platform.jni;

import com.beust.jcommander.Parameter;

/**
 * Created by christian on 3/8/16.
 */
public class Anno4JDependenciesCLI {

    @Parameter(names = { "--version", "-v"}, description = "Prints current Mico platform version. Conflicts with --anno4jversion.")
    private boolean version;

    @Parameter(names = { "--anno4jversion", "-av"}, description = "Prints Version of anno4j used. Conflicts with --version.")
    private boolean a4jversion;

    public boolean isVersion() {
        return version;
    }

    public boolean isA4jversion() {
        return a4jversion;
    }
}
