# script for adding java projects jnipp and javadeps as tools to the anno4cpp project
# In order to work properly the user must have access to the two git repositories 
# 
# must be included into anno4cpp main cmake first!

include(ExternalProject)

ExternalProject_Add(javadeps
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/externals/javadeps
    GIT_REPOSITORY git@bitbucket.org:mico-project/javadeps.git
    GIT_TAG javadeps-1.0
    CONFIGURE_COMMAND ""
    BUILD_COMMAND "" 
    INSTALL_COMMAND ""
)
ExternalProject_Add(jnipp
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/externals/jnipp
    GIT_REPOSITORY git@bitbucket.org:mico-project/jnipp.git
    GIT_TAG jnipp-1.0
    CONFIGURE_COMMAND ""
    BUILD_COMMAND "" 
    INSTALL_COMMAND ""
)
ExternalProject_Add_Step(javadeps MVN_BUILD
  COMMAND mvn clean package
  DEPENDEES download
  WORKING_DIRECTORY <SOURCE_DIR>
)
ExternalProject_Add_Step(jnipp MVN_BUILD
  COMMAND mvn clean package
  DEPENDEES download
  WORKING_DIRECTORY <SOURCE_DIR>
)

ExternalProject_Get_Property(javadeps SOURCE_DIR)
get_filename_component(JAVADEPS_BIN_DIR ${SOURCE_DIR}/target ABSOLUTE)

ExternalProject_Get_Property(jnipp SOURCE_DIR)
get_filename_component(JNIPP_BIN_DIR ${SOURCE_DIR}/target ABSOLUTE)
