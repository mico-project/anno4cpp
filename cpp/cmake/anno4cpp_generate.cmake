# script for running dependency resolver for anno4cpp
#
# requires:
# JAVADEPS_BIN_DIR             - to be set to the path where the javadeps jar resides
# JNIPP_BIN_DIR                - to be set to the path where the jnipp jar resides
# JNIPP_GEN_DIR                - to be set to the path where the cpp files should be generated into
# JNIPP_GEN_PREFIX             - prefix of the generated files
# ANNO4JDEPS_MVN_ROOT          - to be set to the path where the anno4jdependencies pom.xml resides
# FORCE_GENERATION             - if set, generation will always be enforced


##################################### STATIC DEFS #########################################

set (PREFIXES_ALL_CLASSES "org.openrdf.repository.object;eu.mico.platform.persistence;com.github.anno4j;eu.mico.platform.anno4j")
set (PREFIXES_USED_CLASSES "java;org.openrdf.model.impl;org.openrdf.repository.config;org.openrdf.repository.sparql")
set (ADDITIONAL_CLASSES "java.lang.Integer;java.lang.Double;java.util.Set;java.util.List;java.util.ArrayList;java.util.HashMap;java.util.HashSet;org.openrdf.repository.object.ObjectConnection;org.openrdf.repository.object.ObjectRepository;org.openrdf.idGenerator.IDGenerator;org.openrdf.sail.memory.model.MemValueFactory")


##################################### INPUT CHECKS ########################################

if (NOT EXISTS ${JAVADEPS_BIN_DIR})
  message(FATAL_ERROR "javadeps directory does not seem to exists.")
endif()

if (NOT EXISTS ${JAVADEPS_BIN_DIR})
  message(FATAL_ERROR "javadeps directory does not seem to exists.")
endif()

if (NOT EXISTS ${ANNO4JDEPS_MVN_ROOT})
  message(FATAL_ERROR "anno4jdependencies maven root does not seem to exists.")
endif()

#find the current jar in the javadeps build dir
file(GLOB JAVADEPS_JARS RELATIVE "${JAVADEPS_BIN_DIR}" "${JAVADEPS_BIN_DIR}/javadeps-*.jar")
list(LENGTH JAVADEPS_JARS NUM_ELEM)
if (${NUM_ELEM} MATCHES "0")
  message(FATAL_ERROR "Could not find any javadeps jar.")
endif()
list(GET JAVADEPS_JARS 0 JAVADEPS_JAR)
get_filename_component(JAVADEPS ${JAVADEPS_BIN_DIR}/${JAVADEPS_JAR} ABSOLUTE)
message(STATUS "Using javadeps jar          : " ${JAVADEPS})

#find the current jar in the jnipp build dir
file(GLOB JNIPP_JARS RELATIVE "${JNIPP_BIN_DIR}" "${JNIPP_BIN_DIR}/jnipp-*.jar")
list(LENGTH JNIPP_JARS NUM_ELEM)
if (${NUM_ELEM} MATCHES "0")
  message(FATAL_ERROR "Could not find any jnipp jar.")
endif()
list(GET JNIPP_JARS 0 JNIPP_JAR)
get_filename_component(JNIPP ${JNIPP_BIN_DIR}/${JNIPP_JAR} ABSOLUTE)
message(STATUS "Using jnipp jar             : " ${JNIPP})
#########################################################################################

# build the current dependency jar
file(GLOB ANNODEPS_JARS RELATIVE "${ANNO4JDEPS_MVN_ROOT}/target" "${ANNO4JDEPS_MVN_ROOT}/target/anno4jdependencies-*.jar")
list(LENGTH ANNODEPS_JARS NUM_ELEM)
if (${NUM_ELEM} MATCHES "0" OR FORCE_GENERATION)
  message(STATUS "Could not find any anno4jdependencies jar or generation enforced - building.")
  execute_process(
    COMMAND mvn package 
    WORKING_DIRECTORY ${ANNO4JDEPS_MVN_ROOT}
    RESULT_VARIABLE GEN_RESULT_CODE
    OUTPUT_VARIABLE OUTPUT_MESSAGE
    ERROR_VARIABLE OUTPUT_MESSAGE
  )
  if (NOT ${GEN_RESULT_CODE} MATCHES 0)
       message(FATAL_ERROR "Building anno4jdependencies failed with message: " ${OUTPUT_MESSAGE})
  endif()
endif()

file(GLOB ANNODEPS_JARS RELATIVE "${ANNO4JDEPS_MVN_ROOT}/target" "${ANNO4JDEPS_MVN_ROOT}/target/anno4jdependencies-*.jar")
list(LENGTH ANNODEPS_JARS NUM_ELEM)
if (${NUM_ELEM} MATCHES "0")
  message(FATAL_ERROR "Build of anno4jdependencies jar failed.")
endif()
list(GET ANNODEPS_JARS 0 ANNODEPS_JAR)
get_filename_component(ANNODEPS_JAR ${ANNO4JDEPS_MVN_ROOT}/target/${ANNODEPS_JAR} ABSOLUTE)

# get current anno4jversion
execute_process(
  COMMAND java -jar ${ANNODEPS_JAR} --anno4jversion
  RESULT_VARIABLE DEPS_RESULT_CODE
  OUTPUT_VARIABLE OUTPUT_MESSAGE
  ERROR_VARIABLE OUTPUT_MESSAGE
)
if (NOT ${DEPS_RESULT_CODE} MATCHES 0)
    message(FATAL_ERROR "version retrieval failed with message: " ${OUTPUT_MESSAGE})
endif()

set(MMMANNO4J_VERSION ${OUTPUT_MESSAGE})
message(STATUS "Using anno4jdependencies jar: ${ANNODEPS_JAR}, anno4j version: ${MMMANNO4J_VERSION}")


if (NOT FORCE_GENERATION AND
    EXISTS ${JNIPP_GEN_DIR}/${JNIPP_GEN_PREFIX}.h AND
    EXISTS ${JNIPP_GEN_DIR}/${JNIPP_GEN_PREFIX}.cpp AND
    EXISTS ${JNIPP_GEN_DIR}/jnipp.h)

  message(STATUS "cpp files have been generated already. Done!")
  return()

endif()

message(STATUS "Retrieving dependencies for [${ANNODEPS_JAR}].")

# call javadeps for all anno4j related packages and include all classes
execute_process(
  COMMAND java -jar ${JAVADEPS} -s -p ${PREFIXES_ALL_CLASSES} -cp ${ANNODEPS_JAR} -o ${CMAKE_CURRENT_BINARY_DIR}/all_classes.txt
  #COMMAND java -jar /home/christian/mico/javadeps/target/javadeps-1.0-SNAPSHOT.jar -s -p ${PREFIXES_ALL_CLASSES} -cp ${ANNODEPS_JAR}
  RESULT_VARIABLE DEPS_RESULT_CODE
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  OUTPUT_VARIABLE OUTPUT_MESSAGE
  ERROR_VARIABLE OUTPUT_MESSAGE
)
if (NOT ${DEPS_RESULT_CODE} MATCHES 0)
    message(FATAL_ERROR "javadeps process failed with message: " ${OUTPUT_MESSAGE})
endif()
# call javadeps for all used classes
execute_process(
  COMMAND java -jar ${JAVADEPS} -s -p ${PREFIXES_USED_CLASSES} -cp ${ANNODEPS_JAR} -u eu.mico.platform.jni.Anno4JDependencies -o ${CMAKE_CURRENT_BINARY_DIR}/deps_classes.txt
  #COMMAND java -jar /home/christian/mico/javadeps/target/javadeps-1.0-SNAPSHOT.jar -p ${PREFIXES_USED_CLASSES} -cp ${ANNODEPS_JAR} -u eu.mico.platform.jni.Anno4JDependencies
  RESULT_VARIABLE DEPS_RESULT_CODE
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  OUTPUT_VARIABLE OUTPUT_MESSAGE
  ERROR_VARIABLE ERROR_MESSAGE
)
if (NOT ${DEPS_RESULT_CODE} MATCHES 0)
    message(FATAL_ERROR "javadeps process failed with message: " ${ERROR_MESSAGE})
endif()

#turn output into list
FILE(READ "${CMAKE_CURRENT_BINARY_DIR}/all_classes.txt" ANNO4J_CLASS_LIST)
string(REPLACE "\n" ";" ANNO4J_CLASS_LIST  ${ANNO4J_CLASS_LIST})
FILE(READ "${CMAKE_CURRENT_BINARY_DIR}/deps_classes.txt" ANNO4J_CLASS_LIST2)
string(REPLACE "\n" ";" ANNO4J_CLASS_LIST2  ${ANNO4J_CLASS_LIST2})

foreach(c ${ANNO4J_CLASS_LIST2})
  list(APPEND ANNO4J_CLASS_LIST ${c})
endforeach()

foreach (c ${ADDITIONAL_CLASSES})
  list(APPEND ANNO4J_CLASS_LIST ${c})
endforeach()

# call jnipp and generate cpp sources
set (OUTPUT_MESSAGE "")
#message(STATUS "Generating files [${JNIPP_GEN_DIR}/${JNIPP_GEN_PREFIX}.*] with jnipp.")
#message(STATUS "java -jar ${JNIPP} ${ANNO4J_CLASS_LIST} -d ${JNIPP_GEN_DIR} -p ${JNIPP_GEN_PREFIX} -cp ${ANNODEPS_JAR}")
execute_process(
    COMMAND java -jar ${JNIPP} ${ANNO4J_CLASS_LIST} -d ${JNIPP_GEN_DIR} -p ${JNIPP_GEN_PREFIX} -cp ${ANNODEPS_JAR}
    RESULT_VARIABLE GEN_RESULT_CODE
    OUTPUT_VARIABLE OUTPUT_MESSAGE
    ERROR_VARIABLE ERROR_MESSAGE
)
if (NOT ${GEN_RESULT_CODE} MATCHES 0)
    message(FATAL_ERROR "Generation Process failed with message: " ${ERROR_MESSAGE})
endif()
