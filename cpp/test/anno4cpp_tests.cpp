/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "gtest.h"

#include <string>
#include <memory>

#include <jnipp.h>
#include <anno4cpp.h>

std::string g_marmotta_uri;
std::string g_classpath;

// namespaces defines c++11 aliases for shorten the class names

using namespace jnipp::java::lang;

using namespace jnipp::eu::mico::platform::persistence::impl;

using namespace jnipp::eu::mico::platform::anno4j::model;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::bodymmm;
using namespace jnipp::eu::mico::platform::anno4j::model::impl::targetmmm;

using namespace jnipp::org::openrdf::model::impl;
using namespace jnipp::org::openrdf::repository::sparql;
using namespace jnipp::com::github::anno4j;
using namespace jnipp::com::github::anno4j::model::impl;
using namespace jnipp::com::github::anno4j::model::impl::selector;
using namespace jnipp::com::github::anno4j::model::impl::targets;


class Anno4CppTest : public ::testing::Test {

protected:
  static JNIEnv* env;
  static JavaVM* jvm;

  static jnipp::GlobalRef<Anno4j> g_anno4j;

  virtual void SetUp() {
  }

  virtual void connectMarmotta(std::string marmottaBaseURI) {
      jnipp::LocalRef<String>
          marmottaURI = String::create(marmottaBaseURI);

      jnipp::LocalRef<IDGeneratorAnno4j> gen =
          IDGeneratorAnno4j::construct(marmottaURI);

      jnipp::LocalRef<SPARQLRepository> sparqlRepository =
             SPARQLRepository::construct(
            String::create(marmottaBaseURI + std::string("/sparql/select")),
            String::create(marmottaBaseURI + std::string("/sparql/update")));

      sparqlRepository->initialize();

      ASSERT_TRUE(sparqlRepository->isInitialized());
    }

  virtual void createAnno4j(std::string marmottaBaseURI) {
      jnipp::LocalRef<String>
          marmottaURI = String::create(marmottaBaseURI);

      jnipp::LocalRef<IDGeneratorAnno4j> gen =
          IDGeneratorAnno4j::construct(marmottaURI);

      jnipp::LocalRef<SPARQLRepository> sparqlRepository =
             SPARQLRepository::construct(
            String::create(marmottaBaseURI + std::string("/sparql/select")),
            String::create(marmottaBaseURI + std::string("/sparql/update")));

      sparqlRepository->initialize();

      ASSERT_TRUE(sparqlRepository->isInitialized());

      g_anno4j = Anno4j::construct(sparqlRepository,(jnipp::Ref<OrgOpenrdfIdGeneratorIDGenerator>) gen);

      ASSERT_TRUE(g_anno4j);
  }

  virtual void createItemMMM(std::string marmottaBaseURI) {

	  if(!g_anno4j){
		createAnno4j(marmottaBaseURI);
	  }
	  ASSERT_TRUE(g_anno4j);

	  jnipp::LocalRef<ItemMMM> itemMMM = g_anno4j->createObject(ItemMMM::clazz());
	  ASSERT_TRUE(itemMMM);

	  auto contextfromInject = URIImpl::construct(((jnipp::Ref<ResourceObject>) itemMMM )->getResourceAsString());
	  ASSERT_TRUE(contextfromInject);

	  g_anno4j->persist((jnipp::Ref<ResourceMMM>) itemMMM , contextfromInject);

  }

  virtual void createAudioDemuxAnnotation(std::string marmottaBaseURI){

	  if(!g_anno4j){
		createAnno4j(marmottaBaseURI);
	  }
	  ASSERT_TRUE(g_anno4j);

	  //---------------------------------------------
	  //      input created by the inject tool:
	  //---------------------------------------------

	  //create a new item containing an asset with the correct format

	  jnipp::LocalRef<ItemMMM> itemMMM = g_anno4j->createObject(ItemMMM::clazz());
	  ASSERT_TRUE(itemMMM);

	  jnipp::LocalRef<AssetMMM> assetMMM= g_anno4j->createObject(AssetMMM::clazz());
	  ASSERT_TRUE(assetMMM);

	  assetMMM->setFormat  (String::create("audio/wav"));
	  assetMMM->setLocation(String::create("path/to/storage"));
	  ((jnipp::Ref<ResourceMMM>) itemMMM )->setAsset(assetMMM);

	  // call persist to move item and all generated annotations to corresponding sub-graph

	  auto contextfromInject = URIImpl::construct(((jnipp::Ref<ResourceObject>) itemMMM )->getResourceAsString());
	  ASSERT_TRUE(contextfromInject);
	  g_anno4j->persist(itemMMM, contextfromInject);


	  //---------------------------------------------
	  //      annotation created by the extractor:
	  //---------------------------------------------

	  //every Item should have at least one Part

	  jnipp::LocalRef<PartMMM>   partMMM = g_anno4j->createObject(PartMMM::clazz());
	  ASSERT_TRUE(partMMM);

	  //every part MUST have a body
	  jnipp::LocalRef<AudioDemuxBodyMMM> audioDemuxBody = g_anno4j->createObject(AudioDemuxBodyMMM::clazz());
	  ASSERT_TRUE(audioDemuxBody);

	  audioDemuxBody->setFrameRate(String::create("8000"));

	  //every part MUST (?) have a target with a source
	  jnipp::LocalRef<SpecificResourceMMM>   targetMMM = g_anno4j->createObject(SpecificResourceMMM::clazz());
	  ASSERT_TRUE(targetMMM);

	  targetMMM->setSource(itemMMM);

	  //add to the part the body, the input resource (in this case the item, but may be a part), and the target
	  partMMM->setBody(audioDemuxBody);
	  partMMM->addInput(itemMMM);
	  partMMM->addTarget(targetMMM);

	  //finally link everything to the input
	  itemMMM->addPart(partMMM);


	  //move the part(s) to the same context of the input
	  g_anno4j->persist(itemMMM, contextfromInject);

  }

  virtual void createTVSAnnotation(std::string marmottaBaseURI){

	  if(!g_anno4j){
		createAnno4j(marmottaBaseURI);
	  }
	  ASSERT_TRUE(g_anno4j);

	  //---------------------------------------------
	  //      input created by the inject tool:
	  //---------------------------------------------

	  //create a new item containing an asset with the correct format

	  jnipp::LocalRef<ItemMMM> itemMMM = g_anno4j->createObject(ItemMMM::clazz());
	  ASSERT_TRUE(itemMMM);

	  jnipp::LocalRef<AssetMMM> assetMMM= g_anno4j->createObject(AssetMMM::clazz());
	  ASSERT_TRUE(assetMMM);

	  assetMMM->setFormat  (String::create("video/mp4"));
	  assetMMM->setLocation(String::create("other/path/to/storage"));
	  ((jnipp::Ref<ResourceMMM>) itemMMM )->setAsset(assetMMM);

	  // call persist to move item and all generated annotations to corresponding sub-graph

	  auto contextfromInject = URIImpl::construct(((jnipp::Ref<ResourceObject>) itemMMM )->getResourceAsString());
	  ASSERT_TRUE(contextfromInject);
	  g_anno4j->persist(itemMMM, contextfromInject);


	  //---------------------------------------------
	  //      annotation created by the extractor:
	  //---------------------------------------------

	  // lets say the video consits of three shots

	  double shots[][2] = {{0,1000}, {1040,7240}, {7280,9240}};

	  for (int i=0; i<3; i++) {

		  //create a part for the shot annotation (no media produces)
		  jnipp::LocalRef<PartMMM>  shotPartMMM=g_anno4j->createObject(PartMMM::clazz());
		  ASSERT_TRUE(shotPartMMM);
		  shotPartMMM->setBody(g_anno4j->createObject(TVSShotBodyMMM::clazz()));

		  jnipp::LocalRef<SpecificResourceMMM> shotSpecres = g_anno4j->createObject(SpecificResourceMMM::clazz());
		  ASSERT_TRUE(shotSpecres);
		  jnipp::LocalRef<FragmentSelector> shotSelector = g_anno4j->createObject(FragmentSelector::clazz());
		  ASSERT_TRUE(shotSelector);

		  shotSelector->setTemporalFragment(Double::construct(shots[i][0]),Double::construct(shots[i][1]));

		  shotSpecres->setSelector(shotSelector);
		  shotSpecres->setSource(itemMMM);


		  shotPartMMM->addTarget(shotSpecres);
		  shotPartMMM->addInput(itemMMM);

		  //and add it to the item
		  itemMMM->addPart(shotPartMMM);
		}

		//move the part(s) to the same context of the input
		g_anno4j->persist(itemMMM, contextfromInject);

  }

  virtual void TearDown() {
  }
};

JNIEnv* Anno4CppTest::env = nullptr;
JavaVM* Anno4CppTest::jvm = nullptr;

jnipp::GlobalRef<Anno4j> Anno4CppTest::g_anno4j;


TEST_F(Anno4CppTest, initVM)
{
  std::string JavaClassPath="-Djava.class.path=";
  JavaClassPath +=  g_classpath;

  JavaVMOption options[1];    // JVM invocation options
  options[0].optionString = (char *) JavaClassPath.c_str();

  JavaVMInitArgs vm_args;

  vm_args.version = JNI_VERSION_1_6;              // minimum Java version
  vm_args.nOptions = 1;
  vm_args.options = options;
  vm_args.ignoreUnrecognized = false;

  jint rc = JNI_CreateJavaVM(&Anno4CppTest::jvm, (void**)&Anno4CppTest::env, &vm_args);

  ASSERT_EQ(rc,JNI_OK);
}

TEST_F(Anno4CppTest, connect_marmotta)
{
  jnipp::Env::Scope scope(Anno4CppTest::jvm);
  connectMarmotta("http://micobox154:8080/marmotta");
}

TEST_F(Anno4CppTest, create_anno4j)
{
  jnipp::Env::Scope scope(Anno4CppTest::jvm);
  createAnno4j(g_marmotta_uri);
}

TEST_F(Anno4CppTest, create_itemMMM)
{
  jnipp::Env::Scope scope(Anno4CppTest::jvm);
  createItemMMM(g_marmotta_uri);
}

TEST_F(Anno4CppTest, create_simple_annotation)
{
  jnipp::Env::Scope scope(Anno4CppTest::jvm);
  createAudioDemuxAnnotation(g_marmotta_uri);
}

TEST_F(Anno4CppTest, create_complex_annotation)
{
  jnipp::Env::Scope scope(Anno4CppTest::jvm);
  createTVSAnnotation(g_marmotta_uri);
}



int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    if(argc == 3) {
        g_marmotta_uri = std::string(argv[1]);
        g_classpath = std::string(argv[2]);

        std::cout << "running tests on " << g_marmotta_uri << std::endl;

        return RUN_ALL_TESTS();
    } else {
        std::cerr << "usage: <testcmd> <marmotta_uri> <classpath>" << std::endl;
    }

}
