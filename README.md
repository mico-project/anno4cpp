[TOC]

# Anno4CPP

Project for using Anno4J (https://github.com/anno4j/anno4j) in Cpp code. It uses a modified version of jnipp in order to automatically generate headers from the anno4j java classes. The generation process and dependency management is done by this project.

# Prerequisites

In order to compile Anno4CPP you need to have installed

* JAVA 7 SDK
* Maven
* CMake version > 3


# How to build

* create
